package de.jensd.fx.glyphs.materialstackicons.demo;

import de.jensd.fx.glyphs.materialstackicons.MaterialStackIconView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;

public class StackDemo extends Application {

    private Scene scene;

    @Override
    public void start(Stage primaryStage) throws Exception {
        TilePane root = new TilePane();
        MaterialStackIconView flashNotAllowedIcon = new MaterialStackIconView();
        flashNotAllowedIcon.getStyleClass().add("flash-not-allowed");
        MaterialStackIconView carsNotAllowed = new MaterialStackIconView();
        carsNotAllowed.getStyleClass().add("cars-not-allowed");
        root.getChildren().addAll(carsNotAllowed,
                flashNotAllowedIcon);
        scene = new Scene(root, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
//        primaryStage.setFullScreen(true);
        scene.getStylesheets().add(StackDemo.class.getResource("materialIcons.css").toExternalForm());
    }
}